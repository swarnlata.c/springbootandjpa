package com.springBootStart;


import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import com.springBootStart.model.Customer;
import com.springBootStart.model.CustomerLedger;
import com.springBootStart.model.PaymentSchedules;
import com.springBootStart.repository.CustomerLedgerRepository;
import com.springBootStart.repository.CustomerRepository;
import com.springBootStart.repository.PaymentSchedulesRepository;
import com.springBootStart.services.CustomerOperations;
import com.springBootStart.services.Validations;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class SpringBootAppTest {

	@Mock
	private CustomerRepository customerRepo;

	@Mock
	private CustomerLedgerRepository ledgerRepo;

	@Mock
	private PaymentSchedulesRepository scheduleRepo;
	
	@Mock
	private Validations validation;

	@InjectMocks
	private CustomerOperations customerOperation;
	
	@Captor
	private ArgumentCaptor<CustomerLedger> captorLedger;
	
	@Captor
	private ArgumentCaptor<PaymentSchedules> captorPayment; 
	
	@Captor
	private ArgumentCaptor<Customer> captorCustomer;

	Customer customer;
	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
		customer = new Customer("David", "Paul", "18-02-1997");
	}
	
	// Add customer
	@Test
	public void addCustomerTest() {
		when(customerRepo.save(customer)).thenReturn(customer);
		assertEquals("Customer Added successfully", customerOperation.addCustomer(customer));
		verify(customerRepo, times(1)).save(customer);
	}

	@Test
	public void addCustomerTestWithFirstName() {
		customer = new Customer("David", "", "");
		when(customerRepo.save(customer)).thenReturn(customer);
		assertEquals("Invalid data", customerOperation.addCustomer(customer));
		verify(customerRepo, never()).save(customer);
	}

	@Test
	public void addCustomerTestWithLastName() {
		customer = new Customer("", "David", "");
		when(customerRepo.save(customer)).thenReturn(customer);
		assertEquals("Invalid data", customerOperation.addCustomer(customer));
		verify(customerRepo, never()).save(customer);
	}

	@Test
	public void addCustomerTestWithDob() {
		customer = new Customer("", "", "02-02-1990");
		when(customerRepo.save(customer)).thenReturn(customer);
		assertEquals("Invalid data", customerOperation.addCustomer(customer));
		verify(customerRepo, never()).save(customer);
	}

	@Test
	public void addCustomerTestWithFirstAndLastName() {
		customer = new Customer("David", "Paul", "");
		when(customerRepo.save(customer)).thenReturn(customer);
		assertEquals("Invalid data", customerOperation.addCustomer(customer));
		verify(customerRepo, never()).save(customer);
	}

	@Test
	public void addCustomerTestWithFirstNameAndDob() {
		customer = new Customer("David", "", "02-02-1990");
		when(customerRepo.save(customer)).thenReturn(customer);
		assertEquals("Invalid data", customerOperation.addCustomer(customer));
		verify(customerRepo, never()).save(customer);
	}

	@Test
	public void addCustomerTestWithLastNameAndDob() {
		customer = new Customer("", "David","02-02-1990");
		when(customerRepo.save(customer)).thenReturn(customer);
		assertEquals("Invalid data", customerOperation.addCustomer(customer));
		verify(customerRepo, never()).save(customer);
	}

	@Test
	public void addCustomerTestWithNoValues() {
		customer = new Customer("", "", "");
		when(customerRepo.save(customer)).thenReturn(customer);
		assertEquals("Invalid data", customerOperation.addCustomer(customer));
		verify(customerRepo, never()).save(customer);
	}

	// For updating customer ledger for purchase
	@Test
	public void updateCustomerLedgerForPurchaseTestForFirstPurchase() {
	
		int customerId = 1;
		BigDecimal amount =new BigDecimal("40");
		CustomerLedger ledger = new CustomerLedger();
		PaymentSchedules paymentSchedule = new PaymentSchedules();
		when(customerRepo.findOne(customerId)).thenReturn(customer);
		when(ledgerRepo.findTopBycustomerCustomerIdOrderByLedgerIdDesc(customerId)).thenReturn(null);
		when(ledgerRepo.save(ledger)).thenReturn(ledger);
		when(scheduleRepo.save(paymentSchedule)).thenReturn(paymentSchedule);
		when(validation.amountValidation(amount)).thenCallRealMethod();
		when(validation.customerValidation(customer)).thenCallRealMethod();
	
		assertEquals("Successful purchase", customerOperation.updateCustomerLedgerForPurchase(customerId, amount));
	
		verify(customerRepo, times(1)).findOne(customerId);
		verify(ledgerRepo, times(1)).findTopBycustomerCustomerIdOrderByLedgerIdDesc(customerId);
		verify(ledgerRepo, times(1)).save(captorLedger.capture());
		CustomerLedger actual = captorLedger.getValue();
		assertEquals("Purchase", actual.getType());
		assertEquals(amount.negate(), actual.getAmount());
		verify(scheduleRepo, times(4)).save(captorPayment.capture());
		List<PaymentSchedules> payment = captorPayment.getAllValues();
		assertEquals(1, payment.get(0).getNumber());
		verify(validation, times(1)).amountValidation(amount);
		verify(validation, times(1)).customerValidation(customer);
	}
	
	@Test
	public void updateCustomerLedgerForPurchaseTestForNextPurchase() {
		
		int customerId = 1;
		BigDecimal amount =new BigDecimal("40");
		BigDecimal ledgerAmount= new BigDecimal("10");
		BigDecimal balance = new BigDecimal("-30");
		CustomerLedger ledger = new CustomerLedger(ledgerAmount, "Purchase", customer, balance, "Open");
			
		PaymentSchedules paymentSchedule = new PaymentSchedules();
		when(customerRepo.findOne(customerId)).thenReturn(customer);
		when(ledgerRepo.findTopBycustomerCustomerIdOrderByLedgerIdDesc(customerId)).thenReturn(ledger);
		when(ledgerRepo.save(ledger)).thenReturn(ledger);
		when(scheduleRepo.save(paymentSchedule)).thenReturn(paymentSchedule);
		when(validation.amountValidation(amount)).thenCallRealMethod();
		when(validation.customerValidation(customer)).thenCallRealMethod();
	
		assertEquals("Successful purchase", customerOperation.updateCustomerLedgerForPurchase(customerId, amount));
	
		verify(customerRepo, times(1)).findOne(customerId);
		verify(ledgerRepo, times(1)).findTopBycustomerCustomerIdOrderByLedgerIdDesc(customerId);
		verify(ledgerRepo, times(1)).save(captorLedger.capture());
		CustomerLedger actual = captorLedger.getValue();
		assertEquals("Purchase", actual.getType());
		assertEquals(amount.negate(), actual.getAmount());
		verify(scheduleRepo, times(4)).save(captorPayment.capture());
		List<PaymentSchedules> payment = captorPayment.getAllValues();
		assertEquals(1, payment.get(0).getNumber());
		verify(validation, times(1)).amountValidation(amount);
		verify(validation, times(1)).customerValidation(customer);
		
	}
	
	@Test
	public void updateCustomerLedgerForPurchaseTestWithNoArguments() {
		
		int customerId = 0;
		BigDecimal amount =new BigDecimal("0");
		when(validation.amountValidation(amount)).thenCallRealMethod();
		assertEquals("Invalid amount", customerOperation.updateCustomerLedgerForPurchase(customerId, amount));
		verify(validation, times(1)).amountValidation(amount);
		verify(customerRepo, never()).findOne(customerId);
		verify(ledgerRepo, never()).findTopBycustomerCustomerIdOrderByLedgerIdDesc(customerId);
		verify(ledgerRepo, never()).save(captorLedger.capture());
		verify(scheduleRepo, never()).save(captorPayment.capture());
		verify(validation, never()).customerValidation(customer);
	}
	
	
	@Test
	public void updateCustomerLedgerForPurchaseTestWithOnlyCustomerId() {
		
		int customerId = 1;
		BigDecimal amount =new BigDecimal("0");
		when(validation.amountValidation(amount)).thenCallRealMethod();
		assertEquals("Invalid amount", customerOperation.updateCustomerLedgerForPurchase(customerId, amount));
		verify(validation, times(1)).amountValidation(amount);
		verify(customerRepo,never()).findOne(customerId);
		verify(ledgerRepo, never()).findTopBycustomerCustomerIdOrderByLedgerIdDesc(customerId);
		verify(ledgerRepo, never()).save(captorLedger.capture());
		verify(scheduleRepo, never()).save(captorPayment.capture());
		verify(validation, never()).customerValidation(customer);
		
	}
	
	@Test
	public void updateCustomerLedgerForPurchaseTestWithOnlyWrongCustomerId() {
		
		int customerId = -1;
		BigDecimal amount =new BigDecimal("0");
		when(validation.amountValidation(amount)).thenCallRealMethod();
		assertEquals("Invalid amount", customerOperation.updateCustomerLedgerForPurchase(customerId, amount));
		verify(validation, times(1)).amountValidation(amount);
		verify(customerRepo, never()).findOne(customerId);
		verify(ledgerRepo, never()).findTopBycustomerCustomerIdOrderByLedgerIdDesc(customerId);
		verify(ledgerRepo, never()).save(captorLedger.capture());
		verify(scheduleRepo, never()).save(captorPayment.capture());
		verify(validation, never()).customerValidation(customer);
		
		
	}
	
	@Test
	public void updateCustomerLedgerForPurchaseTestWithOnlyWrongAmount() {
	
		int customerId = 0;
		BigDecimal amount =new BigDecimal("-10");
		when(validation.amountValidation(amount)).thenCallRealMethod();
		assertEquals("Invalid amount", customerOperation.updateCustomerLedgerForPurchase(customerId, amount));
		verify(validation, times(1)).amountValidation(amount);
		verify(customerRepo, never()).findOne(customerId);
		verify(ledgerRepo, never()).findTopBycustomerCustomerIdOrderByLedgerIdDesc(customerId);
		verify(ledgerRepo, never()).save(captorLedger.capture());
		verify(scheduleRepo, never()).save(captorPayment.capture());
		verify(validation, never()).customerValidation(customer);
	}
	
	@Test
	public void updateCustomerLedgerForPurchaseTestWithOnlyAmount() {
		
		int customerId = 0;
		BigDecimal amount = new BigDecimal("40");
		customer = new Customer();
		when(customerRepo.findOne(customerId)).thenReturn(null);
		when(validation.amountValidation(amount)).thenCallRealMethod();
		when(validation.customerValidation(customer)).thenCallRealMethod();
		
		assertEquals("Invalid Customer", customerOperation.updateCustomerLedgerForPurchase(customerId, amount));
		
		verify(customerRepo, times(1)).findOne(customerId);
		verify(validation, times(1)).amountValidation(amount);
		verify(validation, times(1)).customerValidation(captorCustomer.capture());
		verify(ledgerRepo, never()).findTopBycustomerCustomerIdOrderByLedgerIdDesc(customerId);
		verify(ledgerRepo, never()).save(captorLedger.capture());
		verify(scheduleRepo, never()).save(captorPayment.capture());
		
	}
	
	@Test
	public void updateCustomerLedgerForPurchaseTestForWrongCustomerId() {
		
		int customerId = 1;
		BigDecimal amount = new BigDecimal("40");
		customer = new Customer();
		when(customerRepo.findOne(customerId)).thenReturn(null);
		when(validation.amountValidation(amount)).thenCallRealMethod();
		when(validation.customerValidation(customer)).thenCallRealMethod();
		
		assertEquals("Invalid Customer", customerOperation.updateCustomerLedgerForPurchase(customerId, amount));
		
		verify(customerRepo, times(1)).findOne(customerId);
		verify(validation, times(1)).amountValidation(amount);
		verify(validation, times(1)).customerValidation(captorCustomer.capture());
		verify(ledgerRepo, never()).findTopBycustomerCustomerIdOrderByLedgerIdDesc(customerId);
		verify(ledgerRepo, never()).save(captorLedger.capture());
		verify(scheduleRepo, never()).save(captorPayment.capture());
		
	}
	
	@Test
	public void updateCustomerLedgerForPurchaseTestForWrongAmount() {
		int customerId = 1;
		BigDecimal amount =new BigDecimal("-40");
		when(validation.amountValidation(amount)).thenCallRealMethod();
		assertEquals("Invalid amount", customerOperation.updateCustomerLedgerForPurchase(customerId, amount));
		verify(validation, times(1)).amountValidation(amount);
		verify(customerRepo, never()).findOne(customerId);
		verify(ledgerRepo, never()).findTopBycustomerCustomerIdOrderByLedgerIdDesc(customerId);
		verify(ledgerRepo, never()).save(captorLedger.capture());
		verify(scheduleRepo, never()).save(captorPayment.capture());
		verify(validation, never()).customerValidation(customer);
	}
	
	@Test
	public void updateCustomerLedgerForPurchaseTestForWrongCustomerIdAndAmount() {
		int customerId = -1;
		BigDecimal amount =new BigDecimal("-40");
		when(validation.amountValidation(amount)).thenCallRealMethod();
		assertEquals("Invalid amount", customerOperation.updateCustomerLedgerForPurchase(customerId, amount));
		verify(validation, times(1)).amountValidation(amount);
		verify(customerRepo, never()).findOne(customerId);
		verify(ledgerRepo, never()).findTopBycustomerCustomerIdOrderByLedgerIdDesc(customerId);
		verify(ledgerRepo, never()).save(captorLedger.capture());
		verify(scheduleRepo, never()).save(captorPayment.capture());
		verify(validation, never()).customerValidation(customer);
	}
		
	//For updating customer Ledger for payment
	
	@Test
	public void updateCustomerLedgerForPaymentTestWithPendingPayments() {
		
		ArgumentCaptor<Integer> captorCustomerId = ArgumentCaptor.forClass(Integer.class);
		int ledgerId =1;
		int customerId=1;
		BigDecimal amount = new BigDecimal("10");
		BigDecimal mainAmount = new BigDecimal("-40");
		BigDecimal ledgerAmount= new BigDecimal("10");
		BigDecimal balance = new BigDecimal("-30");
		CustomerLedger ledgerPurchase = new CustomerLedger(mainAmount, "Purchase", customer, balance, "Open");
		CustomerLedger ledger = new CustomerLedger(ledgerAmount, "Purchase", customer, balance, "Open");
		PaymentSchedules payment = new PaymentSchedules(ledgerPurchase, 1, "Open", amount);
		when(ledgerRepo.findOne(ledgerId)).thenReturn(ledgerPurchase);
		when(validation.checkingCustomerLedgerStatus(ledgerPurchase)).thenCallRealMethod();
		when(validation.amountValidationForPayment(ledgerPurchase, amount)).thenCallRealMethod();
		when(scheduleRepo.findTopByledgerLedgerIdAndStatusOrderById(ledgerId, "Open")).thenReturn(payment);
		when(ledgerRepo.findTopBycustomerCustomerIdOrderByLedgerIdDesc(customerId)).thenReturn(ledger);
		when(ledgerRepo.save(ledgerPurchase)).thenReturn(ledgerPurchase);
		when(scheduleRepo.save(payment)).thenReturn(payment);
		assertEquals("Payment Completed Successfully", customerOperation.updateCustomerLedgerForPayment(ledgerId, amount));
		
		verify(ledgerRepo, times(1)).findOne(ledgerId);
		verify(validation, times(1)).checkingCustomerLedgerStatus(ledgerPurchase);
		verify(validation, times(1)).amountValidationForPayment(ledgerPurchase, amount);
		verify(scheduleRepo, times(1)).findTopByledgerLedgerIdAndStatusOrderById(ledgerId, "Open");
		verify(ledgerRepo, times(1)).findTopBycustomerCustomerIdOrderByLedgerIdDesc(captorCustomerId.capture());
		verify(ledgerRepo, times(1)).save(captorLedger.capture());
		assertEquals("Payment", captorLedger.getValue().getType());
		assertEquals(amount, captorLedger.getValue().getAmount());
		verify(scheduleRepo, times(1)).save(payment);
	
	}
	
	
	@Test
	public void updateCustomerLedgerForPaymentTestWithPending4thPayment() {
		
		ArgumentCaptor<Integer> captorCustomerId = ArgumentCaptor.forClass(Integer.class);
		int ledgerId =1;
		int customerId=1;
		BigDecimal amount = new BigDecimal("10");
		BigDecimal mainAmount = new BigDecimal("-40");
		BigDecimal ledgerAmount= new BigDecimal("10");
		BigDecimal balance = new BigDecimal("-30");
		CustomerLedger ledgerPurchase = new CustomerLedger(mainAmount, "Purchase", customer, balance, "Open");
		CustomerLedger ledger = new CustomerLedger(ledgerAmount, "Purchase", customer, balance, "Open");
		PaymentSchedules payment = new PaymentSchedules(ledgerPurchase, 4, "Open", amount);
		when(ledgerRepo.findOne(ledgerId)).thenReturn(ledgerPurchase);
		when(validation.checkingCustomerLedgerStatus(ledgerPurchase)).thenCallRealMethod();
		when(validation.amountValidationForPayment(ledgerPurchase, amount)).thenCallRealMethod();
		when(scheduleRepo.findTopByledgerLedgerIdAndStatusOrderById(ledgerId, "Open")).thenReturn(payment);
		when(ledgerRepo.findTopBycustomerCustomerIdOrderByLedgerIdDesc(customerId)).thenReturn(ledger);
		when(ledgerRepo.save(ledgerPurchase)).thenReturn(ledgerPurchase);
		when(scheduleRepo.save(payment)).thenReturn(payment);
		assertEquals("Payment Completed Successfully", customerOperation.updateCustomerLedgerForPayment(ledgerId, amount));
		
		verify(ledgerRepo, times(1)).findOne(ledgerId);
		verify(validation, times(1)).checkingCustomerLedgerStatus(ledgerPurchase);
		verify(validation, times(1)).amountValidationForPayment(ledgerPurchase, amount);
		verify(scheduleRepo, times(1)).findTopByledgerLedgerIdAndStatusOrderById(ledgerId, "Open");
		verify(ledgerRepo, times(1)).findTopBycustomerCustomerIdOrderByLedgerIdDesc(captorCustomerId.capture());
		verify(ledgerRepo, times(2)).save(captorLedger.capture());
		List<CustomerLedger> ledgerList = captorLedger.getAllValues();
		assertEquals("Complete", ledgerList.get(0).getStatus());
		assertEquals("Complete", ledgerList.get(1).getStatus());
		verify(scheduleRepo, times(1)).save(payment);
	}
	
	@Test
	public void updateCustomerLedgerForPaymentTestWithoutPendingPayments() {
		
		ArgumentCaptor<Integer> captorCustomerId = ArgumentCaptor.forClass(Integer.class);
		int ledgerId =1;
		BigDecimal amount = new BigDecimal("10");
		BigDecimal mainAmount = new BigDecimal("-40");
		BigDecimal balance = new BigDecimal("-30");
		CustomerLedger ledgerPurchase = new CustomerLedger(mainAmount, "Purchase", customer, balance, "Complete");
		PaymentSchedules payment = new PaymentSchedules(ledgerPurchase, 4, "Open", amount);

		when(ledgerRepo.findOne(ledgerId)).thenReturn(ledgerPurchase);
		when(validation.checkingCustomerLedgerStatus(ledgerPurchase)).thenCallRealMethod();
		assertEquals("No pending payments for the customerLedger", customerOperation.updateCustomerLedgerForPayment(ledgerId, amount));
		
		verify(ledgerRepo, times(1)).findOne(ledgerId);
		verify(validation, times(1)).checkingCustomerLedgerStatus(ledgerPurchase);
		verify(validation, never()).amountValidationForPayment(ledgerPurchase, amount);
		verify(scheduleRepo, never()).findTopByledgerLedgerIdAndStatusOrderById(ledgerId, "Open");
		verify(ledgerRepo, never()).findTopBycustomerCustomerIdOrderByLedgerIdDesc(captorCustomerId.capture());
		verify(ledgerRepo, never()).save(captorLedger.capture());
		verify(scheduleRepo, never()).save(payment);
	}
	
	@Test
	public void updateCustomerLedgerForPaymentTestWrongLedgerId() {
		
		ArgumentCaptor<Integer> captorCustomerId = ArgumentCaptor.forClass(Integer.class);
		int ledgerId = -1;
		BigDecimal amount = new BigDecimal("10");
		CustomerLedger ledgerPurchase = new CustomerLedger();
		PaymentSchedules payment = new PaymentSchedules(ledgerPurchase, 4, "Open", amount);

		when(ledgerRepo.findOne(ledgerId)).thenReturn(null);
		assertEquals("Ledger id doesn't exist", customerOperation.updateCustomerLedgerForPayment(ledgerId, amount));
		
		verify(ledgerRepo, times(1)).findOne(ledgerId);
		verify(validation, never()).checkingCustomerLedgerStatus(ledgerPurchase);
		verify(validation, never()).amountValidationForPayment(ledgerPurchase, amount);
		verify(scheduleRepo, never()).findTopByledgerLedgerIdAndStatusOrderById(ledgerId, "Open");
		verify(ledgerRepo, never()).findTopBycustomerCustomerIdOrderByLedgerIdDesc(captorCustomerId.capture());
		verify(ledgerRepo, never()).save(captorLedger.capture());
		verify(scheduleRepo, never()).save(payment);
	
	}
	
	@Test
	public void updateCustomerLedgerForPaymentTestWrongAmount() {
		
		ArgumentCaptor<Integer> captorCustomerId = ArgumentCaptor.forClass(Integer.class);
		int ledgerId =1;
		BigDecimal amount = new BigDecimal("-10");
		BigDecimal mainAmount = new BigDecimal("-40");
		BigDecimal balance = new BigDecimal("-30");
		CustomerLedger ledgerPurchase = new CustomerLedger(mainAmount, "Purchase", customer, balance, "Open");
		PaymentSchedules payment = new PaymentSchedules(ledgerPurchase, 4, "Open", amount);

		when(ledgerRepo.findOne(ledgerId)).thenReturn(ledgerPurchase);
		when(validation.checkingCustomerLedgerStatus(ledgerPurchase)).thenCallRealMethod();
		when(validation.amountValidationForPayment(ledgerPurchase, amount)).thenCallRealMethod();
		assertEquals("Amount is not equal to Installment amount", customerOperation.updateCustomerLedgerForPayment(ledgerId, amount));
		
		verify(ledgerRepo, times(1)).findOne(ledgerId);
		verify(validation, times(1)).checkingCustomerLedgerStatus(ledgerPurchase);
		verify(validation, times(1)).amountValidationForPayment(ledgerPurchase, amount);
		verify(scheduleRepo, never()).findTopByledgerLedgerIdAndStatusOrderById(ledgerId, "Open");
		verify(ledgerRepo, never()).findTopBycustomerCustomerIdOrderByLedgerIdDesc(captorCustomerId.capture());
		verify(ledgerRepo, never()).save(captorLedger.capture());
		verify(scheduleRepo, never()).save(payment);
		
	}
	
	@Test
	public void updateCustomerLedgerForPaymentTestWithWrongLedgerIdAndAmount() {
		ArgumentCaptor<Integer> captorCustomerId = ArgumentCaptor.forClass(Integer.class);
		int ledgerId = -1;
		BigDecimal amount = new BigDecimal("10");
		CustomerLedger ledgerPurchase = new CustomerLedger();
		PaymentSchedules payment = new PaymentSchedules(ledgerPurchase, 4, "Open", amount);

		when(ledgerRepo.findOne(ledgerId)).thenReturn(null);
		assertEquals("Ledger id doesn't exist", customerOperation.updateCustomerLedgerForPayment(ledgerId, amount));
		
		verify(ledgerRepo, times(1)).findOne(ledgerId);
		verify(validation, never()).checkingCustomerLedgerStatus(ledgerPurchase);
		verify(validation, never()).amountValidationForPayment(ledgerPurchase, amount);
		verify(scheduleRepo, never()).findTopByledgerLedgerIdAndStatusOrderById(ledgerId, "Open");
		verify(ledgerRepo, never()).findTopBycustomerCustomerIdOrderByLedgerIdDesc(captorCustomerId.capture());
		verify(ledgerRepo, never()).save(captorLedger.capture());	
		verify(scheduleRepo, never()).save(payment);
	}
	
	@Test
	public void updateCustomerLedgerForPaymentTestOnlyAmount() {
	
		ArgumentCaptor<Integer> captorCustomerId = ArgumentCaptor.forClass(Integer.class);
		int ledgerId = 0;
		BigDecimal amount = new BigDecimal("10");
		CustomerLedger ledgerPurchase = new CustomerLedger();
		PaymentSchedules payment = new PaymentSchedules(ledgerPurchase, 4, "Open", amount);

		when(ledgerRepo.findOne(ledgerId)).thenReturn(null);
		assertEquals("Ledger id doesn't exist", customerOperation.updateCustomerLedgerForPayment(ledgerId, amount));
		
		verify(ledgerRepo, times(1)).findOne(ledgerId);
		verify(validation, never()).checkingCustomerLedgerStatus(ledgerPurchase);
		verify(validation, never()).amountValidationForPayment(ledgerPurchase, amount);
		verify(scheduleRepo, never()).findTopByledgerLedgerIdAndStatusOrderById(ledgerId, "Open");
		verify(ledgerRepo, never()).findTopBycustomerCustomerIdOrderByLedgerIdDesc(captorCustomerId.capture());
		verify(ledgerRepo, never()).save(captorLedger.capture());	
		verify(scheduleRepo, never()).save(payment);
		
	}
	
	@Test
	public void updateCustomerLedgerForPaymentTestOnlyLedgerId() {
		
		ArgumentCaptor<Integer> captorCustomerId = ArgumentCaptor.forClass(Integer.class);
		int ledgerId =1;
		BigDecimal amount = new BigDecimal("0");
		BigDecimal mainAmount = new BigDecimal("-40");
		BigDecimal balance = new BigDecimal("-30");
		CustomerLedger ledgerPurchase = new CustomerLedger(mainAmount, "Purchase", customer, balance, "Open");
		PaymentSchedules payment = new PaymentSchedules(ledgerPurchase, 4, "Open", amount);

		when(ledgerRepo.findOne(ledgerId)).thenReturn(ledgerPurchase);
		when(validation.checkingCustomerLedgerStatus(ledgerPurchase)).thenCallRealMethod();
		when(validation.amountValidationForPayment(ledgerPurchase, amount)).thenCallRealMethod();
		assertEquals("Amount is not equal to Installment amount", customerOperation.updateCustomerLedgerForPayment(ledgerId, amount));
		
		verify(ledgerRepo, times(1)).findOne(ledgerId);
		verify(validation, times(1)).checkingCustomerLedgerStatus(ledgerPurchase);
		verify(validation, times(1)).amountValidationForPayment(ledgerPurchase, amount);
		verify(scheduleRepo, never()).findTopByledgerLedgerIdAndStatusOrderById(ledgerId, "Open");
		verify(ledgerRepo, never()).findTopBycustomerCustomerIdOrderByLedgerIdDesc(captorCustomerId.capture());
		verify(ledgerRepo, never()).save(captorLedger.capture());
		verify(scheduleRepo, never()).save(payment);
	}
	
	@Test
	public void updateCustomerLedgerForPaymentTestOnlyWrongAmount() {
		
		ArgumentCaptor<Integer> captorCustomerId = ArgumentCaptor.forClass(Integer.class);
		int ledgerId = 0;
		BigDecimal amount = new BigDecimal("-20");
		CustomerLedger ledgerPurchase = new CustomerLedger();
		PaymentSchedules payment = new PaymentSchedules(ledgerPurchase, 4, "Open", amount);

		when(ledgerRepo.findOne(ledgerId)).thenReturn(null);
		assertEquals("Ledger id doesn't exist", customerOperation.updateCustomerLedgerForPayment(ledgerId, amount));
		
		verify(ledgerRepo, times(1)).findOne(ledgerId);
		verify(validation, never()).checkingCustomerLedgerStatus(ledgerPurchase);
		verify(validation, never()).amountValidationForPayment(ledgerPurchase, amount);
		verify(scheduleRepo, never()).findTopByledgerLedgerIdAndStatusOrderById(ledgerId, "Open");
		verify(ledgerRepo, never()).findTopBycustomerCustomerIdOrderByLedgerIdDesc(captorCustomerId.capture());
		verify(ledgerRepo, never()).save(captorLedger.capture());	
		verify(scheduleRepo, never()).save(payment);
	}
	
	@Test
	public void updateCustomerLedgerForPaymentTestOnlyWrongLedgerId() {
		
		ArgumentCaptor<Integer> captorCustomerId = ArgumentCaptor.forClass(Integer.class);
		int ledgerId = -1;
		BigDecimal amount = new BigDecimal("0");
		CustomerLedger ledgerPurchase = new CustomerLedger();
		PaymentSchedules payment = new PaymentSchedules(ledgerPurchase, 4, "Open", amount);

		when(ledgerRepo.findOne(ledgerId)).thenReturn(null);
		assertEquals("Ledger id doesn't exist", customerOperation.updateCustomerLedgerForPayment(ledgerId, amount));
		
		verify(ledgerRepo, times(1)).findOne(ledgerId);
		verify(validation, never()).checkingCustomerLedgerStatus(ledgerPurchase);
		verify(validation, never()).amountValidationForPayment(ledgerPurchase, amount);
		verify(scheduleRepo, never()).findTopByledgerLedgerIdAndStatusOrderById(ledgerId, "Open");
		verify(ledgerRepo, never()).findTopBycustomerCustomerIdOrderByLedgerIdDesc(captorCustomerId.capture());
		verify(ledgerRepo, never()).save(captorLedger.capture());
		verify(scheduleRepo, never()).save(payment);
	}
	
	//For checking Balance
	
	@Test
	public void checkBalanceTestForCorrectCustomerId() {

		int customerId = 1;
		customer = new Customer("David", "Paul", "18-02-1997");
		BigDecimal ledgerAmount= new BigDecimal("10");
		BigDecimal balance = new BigDecimal("-30");
		CustomerLedger ledger = new CustomerLedger(ledgerAmount, "Purchase", customer, balance, "Open");
		
		when(ledgerRepo.findTopBycustomerCustomerIdOrderByLedgerIdDesc(customerId)).thenReturn(ledger);
		BigDecimal expected = new BigDecimal("-30");
		assertEquals("Balance is -30" ,customerOperation.checkBalance(customerId));
		verify(ledgerRepo, times(1)).findTopBycustomerCustomerIdOrderByLedgerIdDesc(customerId);
	}
	
	@Test
	public void checkBalanceTestForWrongCustomerId() {
		int customerId =-1;
		when(ledgerRepo.findTopBycustomerCustomerIdOrderByLedgerIdDesc(customerId)).thenReturn(null);
		assertEquals("Customer doesn't have any transaction",customerOperation.checkBalance(customerId));
		verify(ledgerRepo, times(1)).findTopBycustomerCustomerIdOrderByLedgerIdDesc(customerId);
	}
	
	//For Open Transaction
	@Test
	public void openTransactionTestCorrectCustomerId() {
		int customerId =1;
		customer = new Customer("David", "Paul", "18-02-1997");
		List<CustomerLedger> ledgerList = new ArrayList<CustomerLedger>();
		BigDecimal ledgerAmount= new BigDecimal("10");
		BigDecimal balance = new BigDecimal("-30");
		CustomerLedger ledger = new CustomerLedger(ledgerAmount, "Purchase", customer, balance, "Open");
		ledger.setLedgerId(1);
		ledgerList.add(ledger);
		when(ledgerRepo.findBycustomerCustomerIdAndStatusAndType(customerId, "Open", "Purchase")).thenReturn(ledgerList);
		List<Integer> expected = Arrays.asList(1);
		assertEquals(expected,customerOperation.openTransaction(customerId));
		verify(ledgerRepo, times(1)).findBycustomerCustomerIdAndStatusAndType(customerId, "Open", "Purchase");
	}
	
	@Test 
	public void openTransactionTestWrongCustomerId() {
		int customerId = -1;
		customer = new Customer("David", "Paul", "18-02-1997");
		List<CustomerLedger> ledgerList = new ArrayList<CustomerLedger>();
		when(ledgerRepo.findBycustomerCustomerIdAndStatusAndType(customerId, "Open", "Purchase")).thenReturn(ledgerList);
		List<Integer> expected = Arrays.asList();
		assertEquals(expected,customerOperation.openTransaction(customerId));
		verify(ledgerRepo, times(1)).findBycustomerCustomerIdAndStatusAndType(customerId, "Open", "Purchase");
	}

}
