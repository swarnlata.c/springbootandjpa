package com.springBootStart;


import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.net.HttpHeaders;
import com.springBootStart.controller.Controller;
import com.springBootStart.model.Customer;
import com.springBootStart.model.CustomerLedger;
import com.springBootStart.model.Payment;
import com.springBootStart.model.Purchase;
import com.springBootStart.repository.CustomerLedgerRepository;
import com.springBootStart.repository.CustomerRepository;
import com.springBootStart.repository.PaymentSchedulesRepository;
import com.springBootStart.services.CustomerOperations;
import com.springBootStart.services.Validations;

import ch.qos.logback.core.util.ContentTypeUtil;

//import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Matchers.any;
import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

import java.math.BigDecimal;

import org.hamcrest.CoreMatchers;
import org.hamcrest.Matchers;

@RunWith(SpringRunner.class)
@SpringBootTest(
		webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
		classes = SpringBootApp.class
		)
@AutoConfigureMockMvc
@TestPropertySource(locations = "classpath:application.properties")
public class ControllerTest {
  
    private static final ObjectMapper om = new ObjectMapper();
    
	@Autowired
	private MockMvc mvc;

	Customer customer = new Customer("David", "Paul", "10-04-1997");
	
	/* Check Balance */
	
	@Test
	public void checkBalanceTest() throws Exception {
		
		mvc.perform( MockMvcRequestBuilders
				.get("/checkBalance/{id}", 6)
				.accept(MediaType.APPLICATION_JSON))
		      	.andDo(print())
		      	.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(content().string("Balance is -40"));
	}

	/*Open transaction */
	
	@Test
	public void openTransactionTest() throws Exception {
		mvc.perform(MockMvcRequestBuilders
				.get("/openTransaction/{id}", 6)
				.accept(MediaType.APPLICATION_JSON))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(jsonPath("$").isArray())
				.andExpect(jsonPath("$[0]").value("11"));
	}
	
	/* Add Customer */
	
	@Test
	public void addCustomerTest() throws Exception {
		String json = om.writeValueAsString(customer);
		mvc.perform(MockMvcRequestBuilders
				.post("/customer")
				.contentType(MediaType.APPLICATION_JSON)
				.content(json)
				.accept(MediaType.APPLICATION_JSON))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(content().string("Customer Added successfully"));
	}

	/* Purchase */
	
	@Test
	public void updateCustomerLedgerForPurchaseIntegrationTest() throws Exception {
		
		BigDecimal amount = new BigDecimal("40");
		Purchase purchase = new Purchase(20,amount);
		String jsonForPurchase = om.writeValueAsString(purchase);
		mvc.perform(MockMvcRequestBuilders
				.post("/purchase")
				.accept(MediaType.APPLICATION_JSON)
				.contentType(MediaType.APPLICATION_JSON)
				.content(jsonForPurchase))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(content().string("Successful purchase"));		
	}
	
	@Test
	public void updateCustomerLedgerForPurchaseIntegrationTestInvalidAmount() throws Exception {
		
		BigDecimal amount = new BigDecimal("-40");
		Purchase purchase = new Purchase(20,amount);
		String jsonForPurchase = om.writeValueAsString(purchase);
		mvc.perform(MockMvcRequestBuilders
				.post("/purchase")
				.accept(MediaType.APPLICATION_JSON)
				.contentType(MediaType.APPLICATION_JSON)
				.content(jsonForPurchase))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(content().string("Invalid amount"));		
	}
	
	@Test
	public void updateCustomerLedgerForPurchaseIntegrationTestInvalidCustomer() throws Exception {
		
		BigDecimal amount = new BigDecimal("40");
		Purchase purchase = new Purchase(40,amount);
		String jsonForPurchase = om.writeValueAsString(purchase);
		mvc.perform(MockMvcRequestBuilders
				.post("/purchase")
				.accept(MediaType.APPLICATION_JSON)
				.contentType(MediaType.APPLICATION_JSON)
				.content(jsonForPurchase))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(content().string("Invalid Customer"));		
	}
	
	@Test
	public void updateCustomerLedgerForPurchaseIntegrationTestWithInvalidCustomerAndAmount() throws Exception {
		
		BigDecimal amount = new BigDecimal("-40");
		Purchase purchase = new Purchase(40,amount);
		String jsonForPurchase = om.writeValueAsString(purchase);
		mvc.perform(MockMvcRequestBuilders
				.post("/purchase")
				.accept(MediaType.APPLICATION_JSON)
				.contentType(MediaType.APPLICATION_JSON)
				.content(jsonForPurchase))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(content().string("Invalid amount"));		
	}
	
	/* Payment */
	
	@Test
	public void updateCustomerLedgerForPaymentTest() throws Exception {
		BigDecimal amount = new BigDecimal("10");
		Payment payment = new Payment(19, amount);
		String jsonForPayment = om.writeValueAsString(payment);
		mvc.perform(MockMvcRequestBuilders
				.post("/payment")
				.accept(MediaType.APPLICATION_JSON)
				.contentType(MediaType.APPLICATION_JSON)
				.content(jsonForPayment))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(content().string("Payment Completed Successfully"));		
	}
	
	@Test
	public void updateCustomerLedgerForPaymentTestIncorrectLedgerId() throws Exception {
		BigDecimal amount = new BigDecimal("10");
		Payment payment = new Payment(30, amount);
		String jsonForPayment = om.writeValueAsString(payment);
		mvc.perform(MockMvcRequestBuilders
				.post("/payment")
				.accept(MediaType.APPLICATION_JSON)
				.contentType(MediaType.APPLICATION_JSON)
				.content(jsonForPayment))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(content().string("Ledger id doesn't exist"));	
		
	}
	
	@Test
	public void updateCustomerLedgerForPaymentTestInvalidAmount() throws Exception {
		BigDecimal amount = new BigDecimal("-10");
		Payment payment = new Payment(19, amount);
		String jsonForPayment = om.writeValueAsString(payment);
		mvc.perform(MockMvcRequestBuilders
				.post("/payment")
				.accept(MediaType.APPLICATION_JSON)
				.contentType(MediaType.APPLICATION_JSON)
				.content(jsonForPayment))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(content().string("Amount is not equal to Installment amount"));	
	}
	
	@Test
	public void updateCustomerLedgerForPaymentTestNoPendingPayments() throws Exception {
		BigDecimal amount = new BigDecimal("10");
		Payment payment = new Payment(18, amount);
		String jsonForPayment = om.writeValueAsString(payment);
		mvc.perform(MockMvcRequestBuilders
				.post("/payment")
				.accept(MediaType.APPLICATION_JSON)
				.contentType(MediaType.APPLICATION_JSON)
				.content(jsonForPayment))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(content().string("No pending payments for the customerLedger"));	
	}
	
	@Test
	public void updateCustomerLedgerForPaymentTestWithWrongLedgerIdAndAmount() throws Exception {
		BigDecimal amount = new BigDecimal("-10");
		Payment payment = new Payment(30, amount);
		String jsonForPayment = om.writeValueAsString(payment);
		mvc.perform(MockMvcRequestBuilders
				.post("/payment")
				.accept(MediaType.APPLICATION_JSON)
				.contentType(MediaType.APPLICATION_JSON)
				.content(jsonForPayment))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(content().string("Ledger id doesn't exist"));	
	}
}
  

