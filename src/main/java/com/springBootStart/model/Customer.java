package com.springBootStart.model;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import io.swagger.annotations.ApiModelProperty;

@Entity
public class Customer {
	@Id 
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@ApiModelProperty(notes = "Unique id of customer",name="customer Id",required = false)
	private int customerId;
	
	@ApiModelProperty(notes = "First name of customer",name="firstName",required = true)
	private String firstName;
	
	@ApiModelProperty(notes = "Last name of customer",name="lastName",required = true)
	private String lastName;
	
	@ApiModelProperty(notes = "date of Birth of customer",name="Date of Birth",required = true, value="dd/mm/yyyy")
	private String dob;

	public Customer() {
		
	}
	
	public Customer(String firstName, String lastName, String dob) {
		this.firstName=firstName;
		this.lastName=lastName;
		this.dob=dob;
	}
	
	
	public int getCustomerId() {
		return customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getDob() {
		return dob;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}

}
