package com.springBootStart.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class PaymentSchedules {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	
	private int id;
	
	@ManyToOne
	private CustomerLedger ledger;
	
	@Column(name="number")
	private int number;
	
	@Column(name="status")
	private String status;
	
	@Column(name="amount")
	private BigDecimal amount;

	public PaymentSchedules() {
		
	}
	public PaymentSchedules(CustomerLedger ledger,int number, String status, BigDecimal amount) {
		this.ledger=ledger;
		this.number=number;
		this.status=status;
		this.amount=amount;
	}
	public CustomerLedger getLedger() {
		return ledger;
	}
	
	public void setLedger(CustomerLedger ledger) {
		this.ledger = ledger;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

}
