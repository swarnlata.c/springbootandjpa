package com.springBootStart.model;

import java.math.BigDecimal;

import io.swagger.annotations.ApiModelProperty;

public class Purchase {
	
	@ApiModelProperty(notes = "Customer id of the customer",name="customer Id",required = true)
	private int customerid;
	
	@ApiModelProperty(notes = "Amount of the Purchase",name="amount",required = true)
	private BigDecimal amount;
	
	public Purchase() {
		
	}
	
	public Purchase(int customerid, BigDecimal amount) {
		this.customerid = customerid;
		this.amount = amount;
	}

	public int getCustomerid() {
		return customerid;
	}
	
	public void setCustomerid(int customerid) {
		this.customerid = customerid;
	}
	
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

}
