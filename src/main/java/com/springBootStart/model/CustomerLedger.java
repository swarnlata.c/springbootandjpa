package com.springBootStart.model;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class CustomerLedger {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	
	private int ledgerId;
	
	private BigDecimal amount;
	
	private String type;
	
	@ManyToOne
	private Customer customer;
	
	private BigDecimal balance;
	
	private String status;

	public CustomerLedger() {
		
	}

	public CustomerLedger(BigDecimal amount, String type, Customer customer, BigDecimal balance, String status) {
		this.amount = amount;
		this.type = type;
		this.customer = customer;
		this.balance = balance;
		this.status = status;
	}


	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	
	public Customer getCustomer() {
		return customer;
	}
	
	public int getLedgerId() {
		return ledgerId;
	}

	public void setLedgerId(int ledgerId) {
		this.ledgerId = ledgerId;
	}

	public BigDecimal getBalance() {
		return balance;
	}

	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
}
