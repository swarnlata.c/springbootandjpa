package com.springBootStart.model;

import java.math.BigDecimal;

import io.swagger.annotations.ApiModelProperty;

public class Payment {
	
	@ApiModelProperty(notes = "Ledger id of the Purchase",name="ledger Id",required = true)
	private int ledgerId;
	
	@ApiModelProperty(notes = "Amount to be payed for installment",name="amount",required = true)
	private BigDecimal amount;
	
	public Payment() {
		
	}
	
	public Payment(int ledgerId, BigDecimal amount) {
		this.ledgerId = ledgerId;
		this.amount = amount;
	}
	
	public int getLedgerId() {
		return ledgerId;
	}
	public void setLedgerId(int ledgerId) {
		this.ledgerId = ledgerId;
	}
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

}
