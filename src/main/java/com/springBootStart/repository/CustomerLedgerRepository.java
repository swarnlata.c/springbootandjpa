package com.springBootStart.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.springBootStart.model.CustomerLedger;

public interface CustomerLedgerRepository extends CrudRepository<CustomerLedger, Integer>{

	public CustomerLedger findTopBycustomerCustomerIdOrderByLedgerIdDesc(int id);

	public List<CustomerLedger> findBycustomerCustomerIdAndStatusAndType(int id, String status, String type);

}
