package com.springBootStart.repository;
import org.springframework.data.repository.CrudRepository;

import com.springBootStart.model.Customer;

public interface CustomerRepository extends CrudRepository<Customer, Integer> {

}
