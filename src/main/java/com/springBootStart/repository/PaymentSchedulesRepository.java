package com.springBootStart.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.springBootStart.model.PaymentSchedules;

public interface PaymentSchedulesRepository extends CrudRepository<PaymentSchedules, Integer> {

	public PaymentSchedules findTopByledgerLedgerIdAndStatusOrderById(int lid, String status);

}
