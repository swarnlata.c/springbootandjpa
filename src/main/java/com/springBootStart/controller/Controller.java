package com.springBootStart.controller;

import com.springBootStart.model.*;
import com.springBootStart.services.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
@Api(value="Credit card Service controller", description = "Rest API's related to Credit card Service")
@RestController
public class Controller {

	private static final Logger logger = LogManager.getLogger(Controller.class);

	@Autowired
	private CustomerOperations customerOperation;

	@ApiOperation(value = "Adding a new Customer")
	@RequestMapping(method=RequestMethod.POST,value="/customer")
	public String addCustomer( @RequestBody Customer customer) {
		logger.trace("Adding a new Customer");
		return customerOperation.addCustomer(customer);
	}
	
	@ApiOperation(value = "Adding a Purchase for the Customer")
	@RequestMapping(method=RequestMethod.POST, value = "/purchase")
	public String updateCustomerLedgerForPurchase(@RequestBody Purchase purchase) {
		logger.trace("Updating Customer Ledger and Payment Schedule for type 'PURCHASE' for Customer Id :: {} and amount :: {}",purchase.getCustomerid(), purchase.getAmount());
		return customerOperation.updateCustomerLedgerForPurchase(purchase.getCustomerid(), purchase.getAmount()); 
	}
	
	@ApiOperation(value = "Adding Payment for the purchase ledger")
	@RequestMapping(method=RequestMethod.POST, value = "/payment")
	public String updateCustomerLedgerForPayment(@RequestBody Payment payment) {
		logger.trace("Updating Customer Ledger and Payment Schedule for type 'PAYMENT' for ledger id :: {} and amount :: {}",payment.getLedgerId() ,payment.getAmount());
		return customerOperation.updateCustomerLedgerForPayment(payment.getLedgerId(),  payment.getAmount());
	}
	
	@ApiOperation(value = "Checking the latest Balance of the customer")
	@RequestMapping(method=RequestMethod.GET, value = "/checkBalance/{id}")
	public String checkBalance(@PathVariable int id) {
		logger.trace("Checking Balance for customer with id :: {}", id);
		return customerOperation.checkBalance(id);
	}

	@ApiOperation(value = "To check if customer has any Open transaction")
	@RequestMapping(method=RequestMethod.GET, value = "/openTransaction/{id}")
	public List<Integer> openTransaction(@PathVariable int id) {
		logger.trace("Checking if customer with id :: {} has any open transaction",id);
		return customerOperation.openTransaction(id);
	}

}
