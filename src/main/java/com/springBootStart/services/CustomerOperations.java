package com.springBootStart.services;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springBootStart.SpringBootApp;
import com.springBootStart.model.Customer;
import com.springBootStart.model.CustomerLedger;
import com.springBootStart.model.PaymentSchedules;
import com.springBootStart.repository.CustomerLedgerRepository;
import com.springBootStart.repository.CustomerRepository;
import com.springBootStart.repository.PaymentSchedulesRepository;
import com.springBootStart.services.Validations;

@Service
public class CustomerOperations {

	private static final Logger logger = LogManager.getLogger(SpringBootApp.class);

	@Autowired
	private CustomerRepository customerRepo;

	@Autowired
	private CustomerLedgerRepository ledgerRepo;

	@Autowired
	private PaymentSchedulesRepository scheduleRepo;
	
	@Autowired
	private Validations validation;
	
	public String addCustomer(Customer customer) {
		if(customer.getFirstName().isEmpty() || customer.getLastName().isEmpty() || customer.getDob().isEmpty()) {
			return "Invalid data";
		}
		customerRepo.save(customer);
		return "Customer Added successfully";
	
	}

	public String updateCustomerLedgerForPurchase(int customerId, BigDecimal amount) {
		if(!validation.amountValidation(amount)) {
			return "Invalid amount";
		}

		Customer customer = customerRepo.findOne(customerId);
		if(!validation.customerValidation(customer)) {
			logger.debug("The customer id :: {} doesnot exist", customerId);
			return "Invalid Customer";
		}
		CustomerLedger customerLedger = new CustomerLedger();
		logger.trace("Updating Customer Ledger for new 'PURCHASE'");
		customerLedger.setCustomer(customer);
		customerLedger.setAmount(amount.negate());
		customerLedger.setStatus("Open");
		BigDecimal balance= new BigDecimal("0");
		customerLedger.setType("Purchase");
		logger.debug("Customer balance before updating for customer id :: {} is :: {}", customerId, balance);
		balance = calculateBalance(customerId, amount.negate());
		logger.debug("Customer balance after updating for customer id :: {} is ::{}", customerId, balance);
		customerLedger.setBalance(balance);
		ledgerRepo.save(customerLedger);
		logger.debug("Customer purchase added successfully with ledger id :: {}", customerLedger.getLedgerId());
		updatePaymentScheduleForPurchase(amount, customerLedger);
		return "Successful purchase";
	}
	
	public BigDecimal calculateBalance(int customerId, BigDecimal amount) {
		CustomerLedger c = ledgerRepo.findTopBycustomerCustomerIdOrderByLedgerIdDesc(customerId);
		BigDecimal balance;
		if(c==null) {
			balance = amount;
			
		}
		else {
			balance = (amount).add(c.getBalance()); 	
		}
		return balance;
	}

	public void updatePaymentScheduleForPurchase(BigDecimal amount, CustomerLedger customerLedger) {
		logger.trace("Updating Payment Schedule for type 'PURCHASE'");
		for(int i=0;i<4;i++) {
			PaymentSchedules paymentSchedule;
			paymentSchedule = purchase(amount);
			int n=i+1;
			paymentSchedule.setNumber(n);
			paymentSchedule.setLedger(customerLedger);
			scheduleRepo.save(paymentSchedule);
		}
		logger.debug("Payment Schedule updated successfully for type 'PURCHASE' for amount :: {}", amount);
	}

	public PaymentSchedules purchase(BigDecimal amount) {
		logger.trace("Setting amount in PaymentSchedule for type 'PURCHASE' and amount :: {}", amount);
		PaymentSchedules ps = new PaymentSchedules();
		ps.setStatus("Open");
		BigDecimal pay = new BigDecimal("4");
		ps.setAmount(amount.divide(pay));	
		return ps;
	}

	public String updateCustomerLedgerForPayment(int ledgerId, BigDecimal amount) {

		logger.trace("Updating Customer Ledger for type 'PAYMENT' with ledger id :: {} and amount :: {}", ledgerId, amount);
		CustomerLedger customerLedgerPurchase = ledgerRepo.findOne(ledgerId);
		if(customerLedgerPurchase==null) {
			return "Ledger id doesn't exist";
		}
		if(validation.checkingCustomerLedgerStatus(customerLedgerPurchase)) {
			return "No pending payments for the customerLedger";
		}
	
		if(validation.amountValidationForPayment(customerLedgerPurchase, amount)) {
			return "Amount is not equal to Installment amount";
		}
		CustomerLedger customerLedger = new CustomerLedger();
		BigDecimal balance = new BigDecimal("0");
		int number = payment(ledgerId);
		Customer c = customerLedgerPurchase.getCustomer();
		customerLedger.setCustomer(c);
		customerLedger.setStatus("Complete");
		customerLedger.setType("Payment");
		customerLedger.setAmount(amount);
		logger.debug("The balance before updating is :: {}", balance);
		balance = calculateBalance(c.getCustomerId(), amount);
		logger.debug("The balance after updating is :: {}", balance);
		customerLedger.setBalance(balance);
		ledgerRepo.save(customerLedger);
		if(number == 4) {
			completingPurchase(customerLedgerPurchase);
		}
		logger.debug("Customer Payment added successfully with customer ledger id :: {}", customerLedger.getLedgerId());
		return "Payment Completed Successfully";
	}

	public int payment(int ledgerId) {
		int number =0;
		PaymentSchedules ps = scheduleRepo.findTopByledgerLedgerIdAndStatusOrderById(ledgerId,"Open");
		logger.debug("Updating the Payment Schedules for payment with status :: {}",ps.getStatus());
		ps.setStatus("COMPLETE");                                     
		logger.debug("After updating status is :: {}", ps.getStatus());
		number = ps.getNumber();
		scheduleRepo.save(ps);
		return number;
	}
	
	public void completingPurchase(CustomerLedger ledger) {
		ledger.setStatus("Complete");
		ledgerRepo.save(ledger);
		logger.debug("Updated Customer ledger status to 'COMPLETE' when customer completed 4 payments");
	}

	public String checkBalance(int customerId) {
		
		logger.debug("Checking customer balance for customer id :: {}", customerId);
		CustomerLedger c = ledgerRepo.findTopBycustomerCustomerIdOrderByLedgerIdDesc(customerId);
		if(c == null) {
			return "Customer doesn't have any transaction";
		}
		BigDecimal balance = c.getBalance();
		String str = "Balance is " +balance;
		logger.debug("balance for customer id :: {} is :: {}", customerId, balance);
		return str;
	}

	public List<Integer> openTransaction(int customerId) {
		List<Integer> result = new ArrayList<Integer>();
		logger.debug("Checking if the Status is 'OPEN' for type 'PURCHASE' of customer with customer id :: {}", customerId);
		List<CustomerLedger> ledgerList = ledgerRepo.findBycustomerCustomerIdAndStatusAndType(customerId, "Open", "Purchase");
		CustomerLedger ledger;
		Iterator<CustomerLedger> itr = ledgerList.iterator();
		logger.debug("List of ledger id's that are open for customer id :: {}", customerId);
		while(itr.hasNext()) {
			ledger = itr.next();
			result.add(ledger.getLedgerId());
		}
		return result;
	}
}
