package com.springBootStart.services;

import com.springBootStart.model.CustomerLedger;

import java.math.BigDecimal;

import org.springframework.stereotype.Service;

import com.springBootStart.model.Customer;

@Service
public class Validations {

	public boolean customerValidation(Customer customer) {
		if(customer == null) {
			return false;
		}
		return true;
	}
	
	public boolean amountValidation(BigDecimal amount) {
		BigDecimal zero = new BigDecimal("0");
		if(amount.compareTo(zero)== 1) {
			 return true;
		}
		return false;
	}
	
	public boolean amountValidationForPayment(CustomerLedger customerLedger,BigDecimal amount) {
		BigDecimal numberOfInstallments = new BigDecimal("4");
		BigDecimal exactAmount = (customerLedger.getAmount().divide(numberOfInstallments));
		exactAmount = exactAmount.negate();
		if(amount.compareTo(exactAmount) == 0) {
			return false;
		}
		return true;	
	}
	
	public boolean checkingCustomerLedgerStatus(CustomerLedger ledger) {
		
		String status = ledger.getStatus();
		if(status.equals("Open")) {
			return false;
		}
		return true;
	}
}
